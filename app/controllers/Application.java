package controllers;

import models.Comment;
import models.Post;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.io.File;
import java.io.IOException;

public class Application extends Controller {

    static Form<Post> postForm = form(Post.class);
    static Form<Comment> commentForm = form(Comment.class);

    public static Result index() {
        return redirect(routes.Application.posts());
    }

    public static Result posts() {
        return ok(views.html.index.render(Post.all(), postForm));
    }

    public static Result showPost(Long id) {
        Post p = Post.getPost(id);
        return ok(views.html.blogpost.render(p, commentForm));
    }

    public static Result newPost() {
        Http.MultipartFormData body = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart picture = body.getFile("picture");
        Form<Post> filledForm = postForm.bindFromRequest();
        if(picture == null || filledForm.hasErrors()) {
            return badRequest(views.html.index.render(Post.all(), filledForm));
        } else {
            String fileName = picture.getFilename();
            String contentType = picture.getContentType();
            File file = picture.getFile();
            System.out.println("new filename: " + fileName);
            File testFolder = new File(".\\public\\images\\uploads\\");
            if(!testFolder.exists())
                testFolder.mkdir();
            String imageUrl = ".\\public\\images\\uploads\\"+ fileName;
            file.renameTo(new File(imageUrl));
            try {
                System.out.println("worked? " + file.createNewFile());
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("tha file: " + file.toString());
            imageUrl = "images\\uploads\\" + fileName;
            Post.create(imageUrl, filledForm.get());
            return redirect(routes.Application.posts());
        }
    }

    public static Result newComment(Long id) {
        Post p = Post.getPost(id);
        Form<Comment> filledForm = commentForm.bindFromRequest();
        if(filledForm.hasErrors()) {
            return badRequest(views.html.blogpost.render(p, filledForm));
        } else {
            Comment.create(p, filledForm.get());
            return redirect(routes.Application.showPost(id));
        }
    }

    public static Result deletePost(Long id) {
        Post.delete(id);
        return redirect(routes.Application.posts());
    }
}
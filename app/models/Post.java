package models;

import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Post extends Model {
    @Id
    public Long id;
    public String title;
    public Date postedAt;
    public String imageUrl;

    @Lob
    @Basic(fetch = FetchType.EAGER)
    public String content;

    @OneToMany(mappedBy = "post", cascade = CascadeType.ALL)
    public List<Comment> comments;

    public static Finder<Long, Post> find = new Finder(
            Long.class, Post.class
    );

    public Post(String title, String content) {
        this.comments = new ArrayList<Comment>();
        this.title = title;
        this.content = content;
        this.postedAt = new Date();
    }

    public static List<Post> all() {
        return find.all();
    }

    public static Post getPost(Long id) {
        return find.byId(id);
    }

    public static void create(String imageUrl, Post post) {
        post.postedAt = new Date();
        post.imageUrl = imageUrl;
        post.save();
    }

    public static void delete(Long id) {
        find.ref(id).delete();
    }

    public Post addComment(String author, String content) {
        Comment newComment = new Comment(this, author, content);
        newComment.save();
        this.comments.add(newComment);
        this.save();
        return this;
    }
}

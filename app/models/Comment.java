package models;

import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Comment extends Model {
    @Id
    public Long id;
    public String author;
    public Date postedAt;

    @Lob
    @Basic(fetch = FetchType.EAGER)
    public String content;

    @ManyToOne()
    public Post post;

    public Comment(Post post, String author, String content) {
        this.post = post;
        this.author = author;
        this.content = content;
        this.postedAt = new Date();
    }

    public static void create(Post post, Comment comment) {
        comment.postedAt = new Date();
        comment.post = post;
        comment.save();
        post.comments.add(comment);
        post.save();
    }
}
